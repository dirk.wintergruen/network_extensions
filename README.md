# network_extensions


A collections of tools to handle networks changing in time as well as different types of multilayer networks.
The project is still only beta. 

I am working on a improved documentation. See the progress at:


https://readthedocs.org/projects/network-extension/

If you install it via pypi:

    pip install network-extensions

there might be missing dependencies. Not all requirements are added to the setup.py file, yet.

The source code is at 

https://gitlab.gwdg.de/dirk.wintergruen/network_extensions.git


