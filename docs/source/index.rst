.. NetExt1 documentation master file, created by
   sphinx-quickstart on Sat May 11 10:58:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NetExt1's documentation!
===================================
NetExt1 is a package which aims at simplifying the handling of multilayer networks and networks which change over time.
The documentation is still quite rudimentary.

In the center are three packages:

* :py:mod:`network_extensions.igraphx`  extends the functionality of https://igraph.org/python/.
* :py:mod:`network_extensions.multilayer` handles multilayer networks
* :py:mod:`network_extensions.simulations` provides tools to compare dynamic networks with simulations of networks using Barabasis or Erdos-Renyi networks.



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   igraphx
   multilayer
   simulations
   semantic
   structure_simulations

   graph_plotly


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
