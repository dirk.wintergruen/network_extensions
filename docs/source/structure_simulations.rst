.. _structure_simulations:

Result structure of a simulation
================================

result of :py:func:`MultiLayer.simulate_ynw`

TODO



graphproperties structure:
##########################

graphproperties
***************

is dict with two keys x:

"largest_component" and "graph"


graphproperties[x]
******************

is a **Dataframe** with:

years as **index** and type of a graph property as **column**

simulations.simulate_largest_components result structure
########################################################

struct with the keys x:

"properties", "largest_component", "graphs"

properties
**********

struct with the keys x:
"largest_component" and "graphs"

properties[x]
*************

struct with the keys y:

"Erdos_Renyi" and "Barabasi"

properties[x][y]
****************

is a **Dataframe** with:

years as **index** and type of a graph property as **column**


